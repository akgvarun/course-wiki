# Conference Pass Generator

## Problem
- You are running a conference
- The registrations are collected as an excel sheet
    - Name
    - Email
    - Company
- Now you have to create a conference pass for each particpant
- So it can be printed

## Solution ideas
- Desing a template using SVG with all the graphics you want
- Read this as base jinja template
- Fill it using the contents from the CSV/Excel
- Write the output file with candidate's email as the image name, so they are unique